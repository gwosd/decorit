# generate doc
echo 'create documentation'
python -m pdoc --html -o . --template-dir ./config --force ../src/decorit/

mv decorit/* .
rmdir decorit
