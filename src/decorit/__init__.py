"""
.. include:: ../../README.md

"""

from .decorit import (
    alias,
    copy_doc,
    copy_doc_params,
    deprecated,
    logging,
    time,
    run_once,
)

__version__ = '0.1.1'
