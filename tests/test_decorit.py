# -*- coding: utf-8 -*-
"""Tests for the decorators module.

BSD 3-Clause License
Copyright (c) 2020-2021, Daniel Nagel
All rights reserved.

"""
import time
import warnings

import decorit
import pytest


def test_deprecated():
    """Test deprecated warning."""
    # define function
    kwargs = {'msg': 'msg', 'since': '1.0.0', 'remove': '1.2.0'}

    @decorit.deprecated(**kwargs)
    def func():
        return True

    warning_msg = (
        'Calling deprecated function func. {msg}'.format(**kwargs) +
        ' -- Deprecated since version {since}'.format(**kwargs) +
        ' -- Function will be removed starting from {remove}'.format(**kwargs)
    )

    with warnings.catch_warnings():
        warnings.filterwarnings('error')
        try:
            assert func()
        except DeprecationWarning as dw:
            assert str(dw) == warning_msg
        else:
            raise AssertionError()


def test_copy_doc():
    """Test copy doc decorator."""
    docstring = 'Define Example function with docstring.'

    def func():
        """Define Example function with docstring."""
        pass

    # test for function
    @decorit.copy_doc(func)
    def functest():
        pass

    assert functest.__doc__ == docstring


def test_copy_doc_params():
    """Test copy doc decorator."""
    # spacing is taken from copied function, this does not to be treated
    # due to help ignoring it anyway
    docstring = """Define Example function with docstring.

Parameters
        ----------
        var : type
            Var and so on.

        """

    def func():
        """Define Example function with docstring.

        Some further details.

        Parameters
        ----------
        var : type
            Var and so on.

        """
        pass

    # test for function
    @decorit.copy_doc_params(func)
    def functest():
        """Define Example function with docstring."""
        pass

    assert functest.__doc__ == docstring


def test_alias():
    """Test alias decorator."""
    # test for function
    name = 'f'

    @decorit.alias(name)
    def func():
        pass

    try:
        f()
    except NameError:
        raise AssertionError()

    assert f.__doc__ != func.__doc__  # noqa: F821
    assert f.__name__ == name  # noqa: F821


@pytest.mark.parametrize('args, kwargs, signature', [
    (('a'), {}, "'a'"),
    (('a', 'b'), {}, "'a', 'b'"),
    ((), {'a': 5}, 'a=5'),
    ((5, 'b'), {'a': 5}, "5, 'b', a=5"),
])
def test_logging(args, kwargs, signature, capsys):
    """Test logging decorator."""
    # create function for test
    def func(*args, **kwargs):
        return

    func_dec = decorit.logging(func)
    func_dec(*args, **kwargs)

    stdout, stderr = capsys.readouterr()
    assert stdout.strip() == (
        "Calling func({0})\n'func' -> None".format(signature)
    )

    assert func() == func_dec()
    assert func.__doc__ == func_dec.__doc__  # noqa: F821
    assert func.__name__ == func_dec.__name__  # noqa: F821


@pytest.mark.parametrize('secs', [0.1, 1, 2])
def test_time(secs):
    """Test timer decorator."""
    # test for function
    def func():
        """Test docstring."""
        time.sleep(secs)

    func_dec = decorit.time(func)

    assert func() == func_dec()
    assert func.__doc__ == func_dec.__doc__  # noqa: F821
    assert func.__name__ == func_dec.__name__  # noqa: F821
